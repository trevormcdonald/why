#!/bin/env/python
import requests
import random

#fill in these sections according to the README
ghost_site=""
user_id = ""
auth_header = ""

#returns a list of three random questions
def get_n_whys(n):
    with open("why.txt", "r") as why:
        return random.sample(list(why), n)

#payload to send in the request, containing only the new bio
data = {
    "users": [
        {
            "bio": " ".join(get_n_whys(3))
        }
    ]
}

#make the request
r = requests.put("https://"+ghost_site+"/ghost/api/v0.1/users/"+user_id+"/?include=roles",
        json=data,
        headers={'Authorization': auth_header})

print(r.status_code)
