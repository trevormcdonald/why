# why

## Update your bio with important questions

To use this, make a cron job that executes `bio.py`. Every time it runs, three
random questions will be sampled from why.txt. This file is taken from
[xkcd](https://xkcd.com/why.txt) and consists of a number of questions you may
find yourself asking a search engine.

Point the script at your Ghost site, and find your user id and authentication
header, and you are good to go.

To find these parameters, open up the console in your browser and navigate to
the Network section. Reload your user profile page, and find a request going
to a url that looks like `your_site/ghost/api/v0.1/users/me` etc...

The request header of this will have an Authorization key that you need.
The response of this request contains an `id` field with your user id. Using
these will let this script work with your Ghost profile.
